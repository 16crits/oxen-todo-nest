import {
  IsOptional,
  IsString,
} from 'class-validator';

export class AuthDto {
  @IsString()
  @IsOptional()
  emailOrName: string;

  @IsString()
  @IsOptional()
  password: string;
}
