import { ForbiddenException, Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { AuthDto, RegisterDto } from './dto';
import * as argon from 'argon2';
import { PrismaClientKnownRequestError } from '@prisma/client/runtime';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { UserResource } from './resource';

@Injectable()
export class AuthService {
  constructor(
    private prisma: PrismaService,
    private jwt: JwtService,
    private config: ConfigService,
  ) {}
  async signIn(dto: AuthDto) {
    const user = await this.checkUser(dto, true);

    const pwCheck = await argon.verify(
      user.password,
      dto.password,
    );

    if (!user || !pwCheck) {
      throw new ForbiddenException(
        'Incorrect user or password.',
      );
    }
    return this.signToken(user.id, user.email);
  }

  async signUp(dto: RegisterDto) {
    const password = await argon.hash(dto.password);

    try {
      const user = await this.prisma.user.create({
        data: {
          email: dto.email,
          password,
          name: dto.name,
        },
      });

      return this.signToken(user.id, user.email);
    } catch (error) {
      if (error instanceof PrismaClientKnownRequestError) {
        if (error.code === 'P2002') {
          throw new ForbiddenException('Email already exists');
        }
      }
    }
  }

  async checkUser(dto: AuthDto, returnPassword = false) {
    const user = await this.prisma.user.findFirst({
      where: {
        OR: [
          { email: dto.emailOrName },
          { name: dto.emailOrName },
        ],
      },
    });
    !returnPassword && delete user.password;
    return user;
  }

  async signToken(
    userId: Number,
    email: string,
  ): Promise<{ access_token: string; id: Number }> {
    const payload = {
      sub: userId,
      email,
    };

    const secret = this.config.get('JWT_SECRET');

    const token = await this.jwt.signAsync(payload, {
      secret,
      expiresIn: '1d',
      // expiresIn: 30,
    });

    return {
      access_token: token,
      id: userId,
    };
  }
}
