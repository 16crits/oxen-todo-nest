import { Timestamp } from 'rxjs';

export class UserResource {
  id: Number;
  name: string;
  email: string;
  // created_at:Timestamp;
}
