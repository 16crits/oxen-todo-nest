import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  Request,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { TodoDto } from './dto';
import { TodoService } from './todo.service';

@Controller('api/to-dos')
export class TodoController {
  constructor(private todoService: TodoService) {}
  @UseGuards(AuthGuard('jwt'))
  @Get()
  getTodos(
    @Query() queryString: { page: number; todos: string },
  ) {
    return this.todoService.getTodos(
      queryString.page ?? 1,
      queryString.todos,
    );
  }

  @Post()
  createTodo(@Body() dto: TodoDto) {
    return this.todoService.createTodo(dto);
  }

  @Put(':id')
  updateTodo(
    @Body() dto: TodoDto,
    @Param() todoId: { id: string },
  ) {
    return this.todoService.updateTodo(dto, parseInt(todoId.id));
  }

  @Delete(':id')
  deleteTodo(@Param() todoId: { id: string }) {
    return this.todoService.deleteTodo(parseInt(todoId.id));
  }
}
