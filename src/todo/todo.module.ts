import { Module } from '@nestjs/common';
import { TodoService } from './todo.service';
import { TodoController } from './todo.controller';
import { JwtStrategy } from 'src/auth/strategy';

@Module({
  providers: [TodoService, JwtStrategy],
  controllers: [TodoController],
})
export class TodoModule {}
