import { Injectable, Request } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { link } from 'fs';
import { PrismaService } from 'src/prisma/prisma.service';
import { TodoDto } from './dto';

@Injectable()
export class TodoService {
  private take: number;
  private skip: number;
  private activeFilter: Object = {
    AND: [
      {
        dead_line: {
          gte: new Date()
            .toLocaleDateString('en-ZA')
            .replaceAll('/', '-'),
        },
      },
      {
        is_done: false,
      },
    ],
  };
  private inactiveFilter: Object = {
    OR: [
      {
        dead_line: {
          lt: new Date()
            .toLocaleDateString('en-ZA')
            .replaceAll('/', '-'),
        },
      },
      {
        is_done: true,
      },
    ],
  };
  constructor(
    private prisma: PrismaService,
    private config: ConfigService,
  ) {}

  async activeTodos() {
    const todos = await this.prisma.todo.findMany({
      take: this.take,
      skip: this.skip,
      orderBy: {
        dead_line: 'asc',
      },
      where: this.activeFilter,
    });

    return todos;
  }

  async inactiveTodos() {
    const todos = await this.prisma.todo.findMany({
      take: this.take,
      skip: this.skip,
      orderBy: {
        dead_line: 'asc',
      },
      where: this.inactiveFilter,
    });

    return todos;
  }

  async getTodos(pageNo = 1, todosFilter = 'active') {
    this.take = parseInt(this.config.get('PAGINATE_COUNT'));
    this.skip = (pageNo - 1) * this.take;
    const totalItems = await this.prisma.todo.count({
      where:
        todosFilter === 'inactive'
          ? this.inactiveFilter
          : this.activeFilter,
    });
    const totalPage = Math.ceil(totalItems / this.take);
    const todos =
      todosFilter === 'inactive'
        ? await this.inactiveTodos()
        : await this.activeTodos();
    const response = {
      data: todos,
      meta: {
        links: this.createLinks(
          totalPage,
          `http://localhost:3333/api/to-dos?todosFilter=${todosFilter}`,
        ),
      },
    };

    return response;
  }

  createLinks(pages: number, URL: string) {
    let links = [];
    for (let i = 1; i <= pages; i++) {
      links.push(`${URL}page=${i}`);
    }
    return links;
  }

  async createTodo(dto: TodoDto) {
    const todo = await this.prisma.todo.create({
      data: {
        title: dto.title,
        dead_line: dto.deadLine,
        userId: dto.userId,
      },
    });
    return todo;
  }

  async deleteTodo(id: number) {
    const todo = await this.prisma.todo.delete({
      where: {
        id,
      },
    });
    return todo;
  }

  async updateTodo(dto: TodoDto, id: number) {
    const todo = await this.prisma.todo.update({
      where: {
        id,
      },
      data: {
        title: dto.title,
        dead_line: dto.deadLine,
        is_done: dto.isDone ?? false,
      },
    });

    return todo;
  }
}
