/*
  Warnings:

  - Added the required column `is_done` to the `Todo` table without a default value. This is not possible if the table is not empty.
  - Added the required column `title` to the `Todo` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Todo" ADD COLUMN     "is_done" BOOLEAN NOT NULL,
ADD COLUMN     "title" TEXT NOT NULL;
