/*
  Warnings:

  - Added the required column `dead_line` to the `todos` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "todos" ADD COLUMN     "dead_line" TEXT NOT NULL;
